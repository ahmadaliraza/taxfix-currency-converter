
const api = require('./api');

module.exports = (app, dir) => {

  // configure all middlewares, views and api routes

  // hide framework identity - can be a security risk
  app.disable('x-powered-by');

  // configure all rest api endpoints
  app.use('/api', api);

  // express error handler
  app.use('*', (err, req, res, nxt) => {

    res.status(500).json({
      success: false,
      message: 'Internal Server Error',
      err
    });

  });

};

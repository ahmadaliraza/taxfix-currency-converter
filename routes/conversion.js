
const ConvertsionCtrl = require('../controllers/conversion/ConvertsionCtrl');

const express = require('express');

const router = express.Router({
  mergeParams: true
});

// get currency rates
router.get('/currencies', ConvertsionCtrl.getSupportedCurrencies);

// get currency rates
router.get('/rates', ConvertsionCtrl.getConversionRates);

// convert currency amounts without transaction history
router.get('/convert', ConvertsionCtrl.convertAmount);

// convert currency amounts with transaction history
router.post('/convert', ConvertsionCtrl.convertAmountWithTransaction);

// fetch transaction history
router.get('/history', ConvertsionCtrl.getConversionHistory);

module.exports = router;

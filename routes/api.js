
const express = require('express');
const conversion = require('./conversion');

const app = express.Router({ mergeParams: true });

// conversion APIs
app.use('/conversion/', conversion);

app.use('*', (req, res) => {

  res.status(404).json({
    success: false,
    message: 'invalid API'
  });

});

module.exports = app;

class ConversionUtils {

  static getDecimalFactor (number) {

    if (Math.floor(+number) === +number) {

      return 1;

    }

    const count = number.toString().split('.')[1].length || 0;

    return 10 ** count;

  }

  static parsePaginationOptions (params) {

    const options = {
      page: +params.page || 1,
      limit: +params.limit || 10,
      leanWithId: true,
      lean: true,
      sort: {
        updatedAt: -1
      }
    };

    if (params.order) {

      options.sort = {};

      if (params.order[0] === '-') {

        options.sort[(params.order.split('-')[1] || 'updatedAt')] = -1;

      } else {

        options.sort[(params.order.toString() || 'updatedAt')] = 1;

      }

    }

    return options;

  }

}

module.exports = ConversionUtils;

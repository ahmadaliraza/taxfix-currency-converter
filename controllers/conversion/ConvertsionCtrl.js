const ConversionHandler = require('./ConversionHandler');

class ConverterCtrl {

  static async getSupportedCurrencies (req, res) {

    try {

      const currencies = await ConversionHandler.getSupportedCurrencies();

      res.json({ success: true, data: currencies });

    } catch (error) {

      res.json({ success: false, message: error.message });

    }

  }

  static async getConversionRates (req, res) {

    try {

      const rates = await ConversionHandler.getConversionRates(req.query);

      res.json({ success: true, data: rates });

    } catch (error) {

      res.json({ success: false, message: error.message });

    }

  }

  static async convertAmount (req, res) {

    try {

      const result = await ConversionHandler.convertAmount(req.query);

      res.json({ success: true, data: result });

    } catch (error) {

      res.json({ success: false, message: error.message });

    }

  }

  static async convertAmountWithTransaction (req, res) {

    try {

      const result = await ConversionHandler.convertAmount(req.body, true);

      res.json({ success: true, data: result });

    } catch (error) {

      res.json({ success: false, message: error.message });

    }

  }

  static async getConversionHistory (req, res) {

    try {

      const results = await ConversionHandler.getConversionHistory(req.query);

      res.json({ success: true, data: results });

    } catch (error) {

      res.json({ success: false, message: error.message });

    }

  }

}

module.exports = ConverterCtrl;

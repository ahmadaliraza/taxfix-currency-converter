
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MongoSchema = new Schema({

  from: {
    type: String,
    required: true,
    uppercase: true
  },

  to: {
    type: String,
    required: true,
    uppercase: true
  },

  rate: {
    type: Number,
    required: true,
    default: 1
  },

  amount: {
    type: Number,
    required: true,
    default: 1
  },

  status: {
    type: String,
    default: 'success',
    enum: ['success', 'failed']
  }

}, {
  timestamps: true,
  versionKey: false
});

MongoSchema.plugin(require('mongoose-paginate'));

module.exports = mongoose.model('Conversion', MongoSchema);

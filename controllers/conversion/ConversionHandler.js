const ECBHandler = require('../ECB/ECBHandler');
const ConversionUtils = require('./ConversionUtils');
const Conversion = require('./Conversion');

// const CF = 10000;
const DEFAULT_CURRENCY = 'EUR';

class ConversionHandler {

  static async getSupportedCurrencies () {

    const rates = await ECBHandler.getRates();

    return rates.map(r => r.currency);

  }

  static async getConversionRates (params) {

    const rates = await ECBHandler.getRates();

    if (!params || !params.base || params.base === DEFAULT_CURRENCY) {

      return rates;

    }

    const baseRate = rates.find(r => r.currency === params.base.toUpperCase());

    if (!baseRate) {

      throw new Error('currency not supported');

    }

    const convertedRates = rates.map((rate) => {

      if (rate.currency === params.base.toUpperCase()) {

        const CF = ConversionUtils.getDecimalFactor(rate.rate);

        return { currency: DEFAULT_CURRENCY, rate: +((1 / (rate.rate * CF)) * CF).toFixed(4) };

      }

      const CF = ConversionUtils.getDecimalFactor(baseRate.rate);

      return { currency: rate.currency, rate: +((1 / (baseRate.rate * CF)) * (rate.rate * CF)).toFixed(4) };

    });

    return convertedRates;

  }

  static async convertAmount (params, withTransaction = false) {

    if (isNaN(params.amount) || +params.amount === 0) {

      throw new Error('invalid amount');

    }

    let transaction = null;

    if (withTransaction) {

      // create new transaction
      transaction = new Conversion({
        from: params.from || DEFAULT_CURRENCY,
        to: params.to || DEFAULT_CURRENCY,
        amount: +params.amount
      });

    }

    if (!params.from) {

      params.from = DEFAULT_CURRENCY;

    }

    if (!params.to) {

      params.to = DEFAULT_CURRENCY;

    }

    const fromCurrency = params.from.toUpperCase();
    const toCurrency = params.to.toUpperCase();

    if (fromCurrency === DEFAULT_CURRENCY && toCurrency === DEFAULT_CURRENCY) {

      // save transaction with default currency
      (withTransaction && transaction) && await transaction.save();

      return {
        from: fromCurrency,
        to: toCurrency,
        rate: 1,
        amount: +(+params.amount).toFixed(4)
      };

    }

    const rates = await ECBHandler.getRates();

    const currencies = rates.map(c => c.currency);

    if (fromCurrency !== DEFAULT_CURRENCY && !currencies.includes(fromCurrency)) {

      throw new Error(`base currency "${fromCurrency}" not supported`);

    }

    if (toCurrency !== DEFAULT_CURRENCY && !currencies.includes(toCurrency)) {

      throw new Error(`target currency "${toCurrency}" not supported`);

    }

    const convertedRates = await ConversionHandler.getConversionRates({ base: params.from });

    const targetRate = convertedRates.find(r => r.currency === toCurrency);

    if (!targetRate) {

      if (withTransaction && transaction) {

        transaction.status = 'failed';
        await transaction.save();

      }

      throw new Error('conversion failed: target currency rate not found');

    }

    const amountCF = ConversionUtils.getDecimalFactor(params.amount);
    const CF = ConversionUtils.getDecimalFactor(targetRate.rate);

    const convertedAmount = ((targetRate.rate * CF) * (+params.amount * amountCF)) / (CF * amountCF);

    if (withTransaction && transaction) {

      transaction.rate = targetRate.rate;
      await transaction.save();

    }

    return {
      from: fromCurrency, to: toCurrency, rate: targetRate.rate, amount: +convertedAmount.toFixed(4)
    };

  }

  static async getConversionHistory (params) {

    try {

      const opts = ConversionUtils.parsePaginationOptions(params);

      const result = await Conversion.paginate({}, opts);

      return result;

    } catch (error) {

      return {
        docs: [],
        page: 0,
        limit: 0,
        total: 0
      };

    }

  }

}

module.exports = ConversionHandler;

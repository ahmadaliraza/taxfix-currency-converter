const TestcaseHandler = require('./TestcaseHandler');

class TestcaseCtrl {

  static async saveTestCase (req, res) {

    try {

      const tc = await TestcaseHandler.saveTestcase(req.body);

      res.json({ success: true, data: tc });

    } catch (error) {

      res.status(400).json({ success: false, message: error.message });

    }

  }

  static async getAllTestCases (req, res) {

    try {

      const tc = await TestcaseHandler.getAllTestCases();

      res.json({ success: true, data: tc });

    } catch (error) {

      res.status(400).json({ success: false, message: error.message });

    }

  }

  static async deleteTestCase (req, res) {

    try {

      await TestcaseHandler.deleteTestCase(req.params.id);

      res.json({ success: true, data: {} });

    } catch (error) {

      res.status(400).json({ success: false, message: error.message });

    }

  }

}

module.exports = TestcaseCtrl;

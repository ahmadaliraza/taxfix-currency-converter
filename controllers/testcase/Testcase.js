
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ModSchema = new Schema({
  name: {
    type: String,
    required: true
  },

  website: {
    type: String,
    required: true
  },

  steps: [{
    type: mongoose.Schema.Types.Mixed,
    default: {}
  }],

  browser: {
    type: String,
    default: null
  }

}, {
  timestamps: true,
  versionKey: false
});

module.exports = mongoose.model('Testcase', ModSchema);

const Testcase = require('./Testcase');

class TestcaseHandler {

  static async saveTestcase (data) {

    const tc = await Testcase.findOne({ name: data.name }).lean().exec();

    if (tc) {

      throw new Error('test case already exist with same name');

    }

    const newTc = new Testcase(data);

    await newTc.validate();

    await newTc.save();

    return newTc;

  }

  static async getAllTestCases () {

    const tcs = await Testcase.find({}).lean().exec();

    return tcs;

  }

  static async deleteTestCase (id) {

    await Testcase.remove({ _id: id }).exec();

  }

  static getTestcaseById (id) {

    return Testcase.findOne({ _id: id }).lean().exec();

  }

}

module.exports = TestcaseHandler;

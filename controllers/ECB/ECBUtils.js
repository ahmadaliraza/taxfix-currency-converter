
const config = require('config');
const request = require('request-promise-native');
const xml2js = require('xml2js');

const { promisify } = require('util');

class ECBUtils {

  static async fetchECBRates () {

    const ecbUrl = config.get('ecb-url');

    try {

      const xmlResponse = await request.get(ecbUrl);

      const parser = new xml2js.Parser();

      const jsonData = await promisify(parser.parseString).call(parser, xmlResponse);

      const rawCurrencies = jsonData['gesmes:Envelope'].Cube[0].Cube[0].Cube;

      const currenciesRates = rawCurrencies.map(curr => ({ currency: curr.$.currency, rate: +curr.$.rate }));

      return currenciesRates;

    } catch (error) {

      return [];

    }

  }

}

module.exports = ECBUtils;

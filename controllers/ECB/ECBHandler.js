const ECBUtils = require('./ECBUtils');

class ECBHandler {

  static async getRates () {

    return ECBUtils.fetchECBRates();

  }

}

module.exports = ECBHandler;

const ReplayHandler = require('./ReplayHandler');

class ReplayCtrl {

  static async startReplay (req, res) {

    try {

      if (!req.body.id) {

        throw new Error('invalid testcase id');

      }

      await ReplayHandler.startReplay(req.body.id);

    } catch (error) {

      res.status(400).json({ success: false, message: error.message });

    }

  }

}

module.exports = ReplayCtrl;

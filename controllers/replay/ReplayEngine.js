
const puppeteer = require('puppeteer');

const escapeXpathString = (str) => {

  const splitedQuotes = str.replace(/'/g, `', "'", '`);
  return `concat('${splitedQuotes}', '')`;

};

class ReplayEngine {

  constructor (testcase, steps) {

    this.testcase = testcase;
    this.steps = steps.map((s) => {

      if (s.screen && s.screen.screenshot) {

        delete s.screen.screenshot;

      }

      return s;

    });

    this.browser = null;
    this.page = null;

  }

  async launch () {

    const launchStep = this.steps.find(s => s.type === 'launch');

    if (!launchStep) {

      console.log(this.steps);

      throw new Error('no launch event found');

    }

    if (!launchStep.screen || !launchStep.screen.href) {

      throw new Error('no launch url found');

    }

    const launchUrl = launchStep.screen.href;

    this.browser = await puppeteer.launch({
      headless: false
    });

    this.page = await this.browser.newPage();

    await this.page.setViewport({
      width: 1600,
      height: 1000
    });

    await Promise.all([

      this.page.goto(launchUrl),
      this.page.waitForNavigation({ waitUntil: 'load' })
    ]);

  }

  async replay () {

    for (const step of this.steps) {

      if (step.type === 'launch' || step.type === 'end') {

        continue;

      }

      console.log('Step', step.element.text);

      await this.click(step);
      // await this.page.waitForNavigation();

    }

    this.stop();

  }

  async stop () {

    this.browser.close();

  }

  async click (step) {

    // const className = step.element.className || '';

    const id = step.element.id || '';

    if (id.trim()) {

      await Promise.all([

        this.page.click(`#${id}`),
        this.page.waitForNavigation({ waitUntil: 'load' })
      ]);

      return console.log('clicked by ID', id);

    }

    const escapedText = escapeXpathString(step.element.text);

    console.log('Clicking by Text', escapedText);

    const xpath = `//*[contains(text(), ${escapedText})]`;

    const result = await this.page.$x(xpath);

    if (result && result.length > 0) {

      await Promise.all([
        result[0].click(),
        this.page.waitForNavigation({ waitUntil: 'load' })
      ]);

    } else {

      console.log('Element not found', step.element.text);

    }

    // });

  }

}

module.exports = ReplayEngine;

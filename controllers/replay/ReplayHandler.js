const TestcaseHandler = require('../testcase/TestcaseHandler');
const ReplayEngine = require('./ReplayEngine');

class ReplayHandler {

  static async startReplay (id) {

    const tc = await TestcaseHandler.getTestcaseById(id);

    if (!tc) {

      throw new Error('test case not available');

    }

    const replEngine = new ReplayEngine(id, tc.steps);

    await replEngine.launch();

    await replEngine.replay();

  }

}

module.exports = ReplayHandler;

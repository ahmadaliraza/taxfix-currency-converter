
const config = require('config');
const express = require('express');

const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// Express for REST APIs
const app = express();

const http = require('http').Server(app);

app.use(bodyParser.json({}));

app.use(bodyParser.urlencoded({ extended: true }));

app.set('json spaces', 2);

// configure API endpoints
const routes = require('./routes');

routes(app, __dirname);

// handle 404 requests
app.use('*', (req, res) => {

  res.status(404).json({ message: 'invalid endpoint' });

});

const options = {
  useMongoClient: true
};

const dbConfig = config.get('database');

const dbConnectionURL = `mongodb://${dbConfig.get('userName') !== '' && dbConfig.get('password') !== '' ? `${dbConfig.get('userName')}:${dbConfig.get('password')}@` : ''}${dbConfig.get('host')}:${dbConfig.get('port')}/${dbConfig.get('dbName')}`;

mongoose.Promise = global.Promise;
mongoose.connect(dbConnectionURL, options, (err) => {

  if (err) {

    console.error('ERR:: connecting database', err);

  } else {

    startServer();
    console.log(`connected to database ${dbConfig.host}:${dbConfig.port}/${dbConfig.dbName}`);

  }

});

const startServer = () => {

  /* Start the Server */
  const server = http.listen(config.port, (err) => {

    if (err) {

      return console.log('ERR:: launching server ', err);

    }

    console.log(`server is live at ${config.host}:${config.port}`);

  });

  server.timeout = 4 * 60 * 1000;

};

process.on('uncaughtException', (err) => {

  console.log('Uncaught Exception thrown', err.stack);

});
process.on('unhandledRejection', (reason, p) => {

  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason.stack);

});

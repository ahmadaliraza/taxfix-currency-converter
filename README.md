# TaxFix Technical Test
this is implementation of test tasks using Nodejs, Express and MongoDB

## Setup

clone the project on local environment

## Start Server

**Docker**

run following command in project directory to start the docker containers

```bash
$ docker-compose up --build
```

it may take a while to build and setup the images in first run. after that it should spin up in no time


**local server**

for local run, environment should have Node8 with NPM installed.

navigate to project and install the dependencies

```bash
$ npm install
```

change the `database` configs to point to a local or remote running MongoDB server in `config/default.json` file and start  the server.

```bash
$ node app.js
```

server will be live and available on port `3000` by default. port can be changed from config file as well

## APIs

base url for all APIs is `http://localhost:<port>` to access the server

**fetch supported currencies**

retrieves the supported currencies for conversion 

`GET /api/conversion/currencies` 

**fetch currency rate**

retrieves the updated ECB currency rates. if `base` currency is not provided, default will be `EUR`. 

`GET /api/conversion/rates?base=<CURRENCY>` 

**conversion without saving transaction history**

 inter converts the amounts of one currency to other using the latest ECB currency rates. if `from` or `to` currency is not provided, default will be `EUR`.

`GET /api/conversion/convert?from=<CURRENCY>&&to=<CURRENCY>&amount=1`

**conversion with a new transaction history**

conversion API with `POST` flavor to save converion transaction. all parameters will be provided in request body.

`POST /api/conversion/convert` 

**get conversion transaction history**

fetches all conversion transactions in pagination format. update `page` and `limit` values to fetch paginated records.

`GET /api/conversion/history?page=1&limit=10` 


## Unit Tests
no unit tests written 


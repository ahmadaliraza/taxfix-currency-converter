FROM node:8

USER root

ENV HOME=/usr/local/taxfix

RUN mkdir -p $HOME

WORKDIR $HOME

COPY package.json /usr/local/taxfix/

RUN npm install

COPY ./ $HOME

RUN ["chmod", "+x", "/usr/local/taxfix/wait-for-it.sh"]

RUN npm install

EXPOSE 3000

CMD ["node", "app.js"]
